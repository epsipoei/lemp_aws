#!/bin/bash

#variables env AWS
echo "your AWS_ACCESS_KEY_ID: "
read userInput
export AWS_ACCESS_KEY_ID="$userInput" >> ~./bashrc

echo "your AWS_SECRET_ACCESS_KEY: "
read userInput
export AWS_SECRET_ACCESS_KEY="$userInput" >> ~./bashrc

echo "your AWS_vpc_id: "
read userInput
export TF_VAR_vpc_id="$userInput" >> ~./bashrc

source ~/.bashrc

#terraform
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform

#python
sudo apt-get update && sudo apt-get install python3 pip

#ansible
python3 -m pip install --user ansible
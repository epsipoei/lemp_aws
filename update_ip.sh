 #!/bin/bash

echo "Script started"

#UPDATE_IP_PUB
new_ip_pub="None"

old_ip_pub=$(grep "mysql_master ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_mysql_master/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/mysql_master ansible_host=$old_ip_pub/mysql_master ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
sed -i "s/= \"$old_ip_pub\"/= \"$new_ip_pub\"/g" Ansible/roles/nginx/files/php/sonde.php
sed -i "s/server_name $old_ip_pub/server_name $new_ip_pub/g" Ansible/roles/nginx/files/cma4.conf
sed -i "s/host=\"$old_ip_pub\"/host=\"$new_ip_pub\"/g" lib/req_get_value_master.py
sed -i "s/mysql_ip_pub=\"$old_ip_pub\"/mysql_ip_pub=\"$new_ip_pub\"/g" lib/ssh_cp.sh

old_ip_pub=$(grep "mysql_slave ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_mysql_slave/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/mysql_slave ansible_host=$old_ip_pub/mysql_slave ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

old_ip_pub=$(grep "haproxy_mysql ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_proxy_mysql/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/haproxy_mysql ansible_host=$old_ip_pub/haproxy_mysql ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

old_ip_pub=$(grep "srv_zabbix ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_srv_zabbix/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/srv_zabbix ansible_host=$old_ip_pub/srv_zabbix ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

old_ip_pub=$(grep "my_vpn ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_my_vpn/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/my_vpn ansible_host=$old_ip_pub/my_vpn ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
sed -i "s/ssh -t ubuntu@$old_ip_pub/ssh -t ubuntu@$new_ip_pub/g" lib/menu.sh
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/backup_1/files/wg-mspr.conf
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/backup_2/files/wg-mspr.conf
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/mysql/files/wg-mspr.conf
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/mysql_slave/files/wg-mspr.conf
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/proxy_mysql/files/wg-mspr.conf
sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/zabbix/files/env/wg-mspr.conf

old_ip_pub=$(grep "backup_1 ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_backup_1/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/backup_1 ansible_host=$old_ip_pub/backup_1 ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
sed -i "s/REMOTE_SERVER=\"$old_ip_pub\"/REMOTE_SERVER=\"$new_ip_pub\"/g" Ansible/roles/mysql/files/backup.sh
sed -i "s/backup_1_ip_pub=\"$old_ip_pub\"/backup_1_ip_pub=\"$new_ip_pub\"/g" lib/ssh_cp.sh

old_ip_pub=$(grep "backup_2 ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
new_ip_pub=$(grep '"public_ip":' Terraform/prj_backup_2/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
sed -i "s/backup_2 ansible_host=$old_ip_pub/backup_2 ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

#UPDATE_IP_PRIV

old_ip_priv=$(grep 'prj_mysql_master =' Terraform/hosts | awk -F'prj_mysql_master = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_mysql_master/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/server mysql1 $old_ip_priv:6033 check/server mysql1 $new_ip_priv:6033 check/g" Ansible/roles/proxy_mysql/files/haproxy.cfg
sed -i "s/master_ip=\"$old_ip_priv\"/master_ip=\"$new_ip_priv\"/g" Ansible/roles/mysql_slave/files/mysql.sh
sed -i "s/prj_mysql_master = $old_ip_priv/prj_mysql_master = $new_ip_priv/g" Terraform/hosts

old_ip_priv=$(grep 'prj_mysql_slave =' Terraform/hosts | awk -F'prj_mysql_slave = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_mysql_slave/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/prj_mysql_slave = $old_ip_priv/prj_mysql_slave = $new_ip_priv/g" Terraform/hosts
sed -i "s/mysql_slave ansible_host=$old_ip_priv/mysql_slave ansible_host=$new_ip_priv/g" Ansible/inventory/hosts
sed -i "s/server mysql2 $old_ip_priv:6033 check/server mysql2 $new_ip_priv:6033 check/g" Ansible/roles/proxy_mysql/files/haproxy.cfg

old_ip_priv=$(grep 'prj_proxy_mysql =' Terraform/hosts | awk -F'prj_proxy_mysql = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_proxy_mysql/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/prj_proxy_mysql = $old_ip_priv/prj_proxy_mysql = $new_ip_priv/g" Terraform/hosts

old_ip_priv=$(grep 'prj_srv_zabbix = ' Terraform/hosts | awk -F'prj_srv_zabbix = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_srv_zabbix/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/prj_srv_zabbix = $old_ip_priv/prj_srv_zabbix = $new_ip_priv/g" Terraform/hosts

old_ip_priv=$(grep 'prj_my_vpn =' Terraform/hosts | awk -F'prj_my_vpn = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_my_vpn/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/prj_my_vpn = $old_ip_priv/prj_my_vpn = $new_ip_priv/g" Terraform/hosts

old_ip_priv=$(grep 'prj_backup_1 =' Terraform/hosts | awk -F'prj_backup_1 = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_backup_1/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/hosts allow = $old_ip_priv/hosts allow = $new_ip_priv/g" Ansible/roles/backup_2/files/rsyncd.conf
sed -i "s/REMOTE_SERVER=\"$old_ip_priv\"/REMOTE_SERVER=\"$new_ip_priv\"/g" Ansible/roles/mysql/files/backup.sh
sed -i "s/backup_1_ip_priv=\"$old_ip_priv\"/backup_1_ip_priv=\"$new_ip_priv\"/g" lib/ssh_cp.sh
sed -i "s/prj_backup_1 = $old_ip_priv/prj_backup_1 = $new_ip_priv/g" Terraform/hosts

old_ip_priv=$(grep 'prj_backup_2 =' Terraform/hosts | awk -F'prj_backup_2 = ' '{print $2}')
new_ip_priv=$(grep '"private_ip":' Terraform/prj_backup_2/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
sed -i "s/hosts allow = $old_ip_priv/hosts allow = $new_ip_priv/g" Ansible/roles/backup_1/files/rsyncd.conf
sed -i "s/prj_backup_2 = $old_ip_priv/prj_backup_2 = $new_ip_priv/g" Terraform/hosts
sed -i "s/backup_2_ip_priv=\"$old_ip_priv\"/backup_2_ip_priv=\"$new_ip_priv\"/g" lib/ssh_cp.sh
sed -i "s/backup_2_ip_priv=\"$old_ip_priv\"/backup_2_ip_priv=\"$new_ip_priv\"/g" Ansible/roles/backup_1/files/rsync.sh
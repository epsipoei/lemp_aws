#!/bin/python3

import os
import subprocess

projects = [
    "prj_my_vpn",
    "prj_mysql_master",
    "prj_backup_1",
    "prj_backup_2",  
    "prj_mysql_slave",
    "prj_proxy_mysql",
    #"prj_srv_zabbix",
]

def run_command(command):
    """Exécute une commande shell et gère les erreurs."""
    try:
        subprocess.run(command, check=True, shell=True)
    except subprocess.CalledProcessError as e:
        print(f"Erreur lors de l'exécution de la commande: {command}")
        print(e)
        exit(1)

def dump_database(master_ip_pub, slave_ip_pub):
    # Copie du dump depuis le serveur maître
    dump_command = f"scp ubuntu@{master_ip_pub}:~/dump.sql /tmp/dump.sql"
    run_command(dump_command)

    # Copie du dump vers le serveur esclave
    copy_command = f"scp -o StrictHostKeyChecking=no /tmp/dump.sql ubuntu@{slave_ip_pub}:/tmp"
    run_command(copy_command)

def run_ansible_playbook(root_path, playbook_name, extra_args=[]):
    inventory_path = os.path.join(root_path, "Ansible/inventory")
    playbook_path = os.path.join(root_path, f"Ansible/playbook/{playbook_name}")

    command = [
        "ansible-playbook",
        "-i", inventory_path,
        "-b",
        "--ssh-common-args='-o StrictHostKeyChecking=no'",
        playbook_path,
    ]

    # Ajouter des arguments supplémentaires si nécessaire
    command.extend(extra_args)

    subprocess.run(command)

def install_projects():
    # Récupérer le répertoire racine du script
    root_path = os.path.dirname(os.path.abspath(__file__))
    
    # for project in projects:
    #     project_directory = os.path.join(root_path, f"Terraform/{project}")
        
    #     # Exécute "terraform init" et "terraform apply" dans le répertoire du projet
    #     run_command(f"cd {project_directory} && terraform init")
    #     run_command(f"cd {project_directory} && terraform apply -auto-approve")
    
    # # Exécute le script update_ip.sh
    # update_ip_script = os.path.join(root_path, "update_ip.sh")
    # run_command(f"cd {root_path} && /bin/bash {update_ip_script}")

    # projects_ansible = [
    # #"prj_my_vpn",
    # "prj_mysql_master",
    # # "prj_proxy_mysql",
    # # "prj_srv_zabbix",
    # # "prj_backup_1",
    # # "prj_backup_2",  
    # ]
    
    # for project in projects_ansible:
    #     run_ansible_playbook(root_path, f"deploy_{project}.yml")
    
    # Récupérer les adresses IP des serveurs maître et esclave
    master_ip_pub = os.popen(f'grep "mysql_master ansible_host=" {root_path}/Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" \'{{print $2}}\'').read().strip()
    slave_ip_pub = os.popen(f'grep "mysql_slave ansible_host=" {root_path}/Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" \'{{print $2}}\'').read().strip()

    # Appeler la fonction dump_database pour effectuer le dump
    dump_database(master_ip_pub, slave_ip_pub)
    
    run_ansible_playbook(root_path, "deploy_prj_mysql_slave.yml")
    
    #run_ansible_playbook(root_path, "start_zabbix_agent.yml")
    
    # Active les backup
    # backup_script = os.path.join(root_path, "lib/ssh_cp.sh")
    # run_command(f"/bin/bash {backup_script}")

    
def destroy_projects():
    # Récupérer le répertoire racine du script
    root_path = os.path.dirname(os.path.abspath(__file__))
    
    for project in projects:
        project_directory = os.path.join(root_path, f"Terraform/{project}")
        
        # Exécute "terraform destroy " dans le répertoire du projet
        run_command(f"cd {project_directory} && terraform destroy -auto-approve")

if __name__ == "__main__":

    # Demander le choix de l'utilisateur
    print("Choisissez une option:")
    print("1. Installer (install)")
    print("2. Détruire (destroy)")

    choice = input("Entrez le numéro ou le nom de l'option : ").strip().lower()

    if choice == "1" or choice == "install":
        install_projects()
    elif choice == "2" or choice == "destroy":
        destroy_projects()
    else:
        print("Option invalide. Veuillez choisir 'install' ou 'destroy'.")

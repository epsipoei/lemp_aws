#!/bin/bash

master_ip="10.10.10.3"
replication_user="admin"
password="iopiop"
filename="mysql-bin.000002"
log_pos=16776

# Démarrer le service MySQL
service mysql start

# Liste des utilisateurs à créer
users=("zbx_monitor" "admin" "ro" "rw")

# Boucle pour créer les utilisateurs et configurer la réplication
for user in "${users[@]}"; do
    # Vérifier si l'utilisateur existe déjà
    user_exists=$(mysql -e "SELECT COUNT(*) FROM mysql.user WHERE user = '$user' AND host = '%';" | tail -1)

    # Créer l'utilisateur s'il n'existe pas déjà
    if [ "$user_exists" -eq 0 ]; then
        mysql << EOF

        CREATE USER 'zbx_monitor'@'%' IDENTIFIED BY 'zabbix';
        GRANT REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zbx_monitor'@'%';

        CREATE USER 'admin'@'%' IDENTIFIED BY 'iopiop';
        GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';

        CREATE USER 'ro'@'%' IDENTIFIED BY 'iopiop';
        GRANT SELECT PRIVILEGES ON *.* TO 'ro'@'%';

        CREATE USER 'rw'@'%' IDENTIFIED BY 'iopiop';
        GRANT SELECT, INSERT, UPDATE, DELETE PRIVILEGES ON *.* TO 'rw'@'%';

        FLUSH PRIVILEGES;
EOF

    else
        echo "L'utilisateur '$user' existe déjà. Passer la création."
    fi
done

# Configuration de la réplication sur le serveur esclave
mysql << EOF
STOP SLAVE;
CHANGE MASTER TO MASTER_HOST='$master_ip', MASTER_USER='$replication_user', MASTER_PASSWORD='$password', MASTER_LOG_FILE='$filename', MASTER_LOG_POS=$log_pos;
START SLAVE;
EOF


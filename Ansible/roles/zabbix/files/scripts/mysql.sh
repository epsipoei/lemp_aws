#!/bin/bash

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF
CREATE DATABASE zabbix character set utf8mb4 collate utf8mb4_bin;
CREATE USER 'zabbix'@'%' IDENTIFIED BY 'zabbix';
GRANT ALL PRIVILEGES ON *.* TO 'zabbix'@'%';
FLUSH PRIVILEGES;
SET global log_bin_trust_function_creators = 1;
EOF

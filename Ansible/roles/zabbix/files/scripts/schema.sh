#!/bin/bash

service mysql start

# Copy shema to db_Zabbix
zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -pzabbix zabbix

mysql << EOL
set global log_bin_trust_function_creators = 0;
EOL
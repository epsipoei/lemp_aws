#!/bin/bash

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF
CREATE DATABASE mspr character set utf8mb4 collate utf8mb4_bin;

CREATE USER 'zbx_monitor'@'%' IDENTIFIED BY 'zabbix';
GRANT REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zbx_monitor'@'%';

CREATE USER 'admin'@'%' IDENTIFIED BY 'iopiop';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';

CREATE USER 'ro'@'%' IDENTIFIED BY 'iopiop';
GRANT SELECT PRIVILEGES ON *.* TO 'ro'@'%';

CREATE USER 'rw'@'%' IDENTIFIED BY 'iopiop';
GRANT SELECT, INSERT, UPDATE, DELETE PRIVILEGES ON *.* TO 'rw'@'%';

FLUSH PRIVILEGES;
EOF

touch /var/log/mysql/mysql-bin.log
chown mysql -Rf /var/log/mysql/
service mysql restart

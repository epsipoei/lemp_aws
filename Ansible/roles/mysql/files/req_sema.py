import pymysql
import ping3
import time

# Connexion à la base de données
db = pymysql.connect(
    host="127.0.0.1", port=6033, user="admin", password="iopiop", database="mspr"
)

while True:
    try:
        # Récupération des adresses IP VPN depuis la base de données
        cursor = db.cursor()
        cursor.execute("SELECT num_sonde, adresse_client_VPN FROM COMMUNICATION_VPN")
        results = cursor.fetchall()

        for row in results:
            num_sonde, adresse_client_VPN = row
            if adresse_client_VPN:
                # Vérification de la disponibilité de l'adresse IP VPN
                response_time = ping3.ping(adresse_client_VPN)
                if response_time is not None:
                    # L'adresse IP VPN est accessible
                    etat_sonde = "connectee"
                else:
                    # L'adresse IP VPN n'est pas accessible
                    etat_sonde = "deconnectee"

                # Mise à jour de l'état dans la base de données
                cursor.execute(
                    "UPDATE SONDE SET etat_sonde=%s WHERE num_sonde=%s",
                    (etat_sonde, num_sonde),
                )
                db.commit()

        cursor.close()

        # Attente avant la prochaine vérification (par exemple, toutes les 5 minutes)
        time.sleep(300)  # 300 secondes = 5 minutes

    except KeyboardInterrupt:
        break

# Fermeture de la connexion à la base de données
db.close()

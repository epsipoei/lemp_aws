#!/bin/bash

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF
use mspr;
-- TEST : Insertion des données

-- Insertion des sondes
INSERT INTO SONDE (num_sonde, nom_sonde, cpu_sonde, ram_sonde, id_semaos, etat_sonde) VALUES
(0000000001, 'Sonde 1', 1.5, 4, 1, NULL),
(0000000002, 'Sonde 2', 2.0, 8, 2, NULL),
(0000000003, 'Sonde 3', 2.5, 16, 3, NULL),
(0000000004, 'Sonde 4', 1.0, 2, 1, NULL),
(0000000005, 'Sonde 5', 2.5, 8, 3, NULL),
(0000000006, 'Sonde 6', 2.0, 4, 1, NULL),
(0000000007, 'Sonde 7', 1.5, 8, 2, NULL),
(0000000008, 'Sonde 8', 2.5, 16, 3, NULL),
(0000000009, 'Sonde 9', 1.0, 4, 1, NULL),
(0000000010, 'Sonde 10', 2.0, 8, 2, NULL),
(0000000011, 'Sonde 11', 2.5, 16, 3, NULL),
(0000000012, 'Sonde 12', 1.0, 4, 1, NULL),
(0000000013, 'Sonde 13', 2.5, 8, 3, NULL),
(0000000014, 'Sonde 14', 2.0, 16, 1, NULL),
(0000000015, 'Sonde 15', 1.5, 4, 2, NULL),
(0000000016, 'Sonde 16', 1.5, 4, 2, NULL);


-- Insertion des données pour la table DISQUE_SEMA
INSERT INTO DISQUE_SEMA (num_sonde, type_disque, taille_disque) VALUES
(1, 'SSD', 256),
(1, 'HDD', 1000),
(2, 'SSD', 512),
(3, 'HDD', 500),
(4, 'SSD', 256),
(5, 'HDD', 1000),
(5, 'SSD', 512),
(5, 'HDD', 500);

INSERT INTO SEMAOS (version_semaos, date_creation_os) VALUES
('1.0', '2022-01-01'),
('2.0', '2022-02-01'),
('3.0', '2022-03-01');

-- Insertion des données pour la table SCRIPT
INSERT INTO SCRIPT (nom_script, version_script) VALUES
('Script 1', '1.0'),
('Script 2', '2.0'),
('Script 3', '3.0');

-- Insertion des données pour la table SCRIPT_SONDE
INSERT INTO SCRIPT_SONDE (num_sonde, id_script, date_install_script) VALUES
(1, 1, '2023-01-01'),
(2, 2, '2023-02-01'),
(3, 3, '2023-03-01'),
(4, 1, '2023-04-01'),
(5, 2, '2023-05-01'),
(5, 3, '2023-05-01');

INSERT INTO CLIENT (SIRET, nom_client, adresse_client, prestataire)
VALUES
-- Prestataires
(10000000000000, 'Prestataire A', 'Adresse Prestataire A', NULL),
(20000000000000, 'Prestataire B', 'Adresse Prestataire B', NULL),
(30000000000000, 'Prestataire C', 'Adresse Prestataire C', NULL),
(40000000000000, 'Prestataire D', 'Adresse Prestataire D', NULL),
(50000000000000, 'Prestataire E', 'Adresse Prestataire E', NULL),
-- Clients du Prestataire A
(11111111111111, 'Client Final 1A', 'Adresse Client Final 1A', 1),
(11111111111112, 'Client Final 2A', 'Adresse Client Final 2A', 1),
(11111111111113, 'Client Final 3A', 'Adresse Client Final 3A', 1),
(11111111111114, 'Client Final 4A', 'Adresse Client Final 4A', 1),
(11111111111115, 'Client Final 5A', 'Adresse Client Final 5A', 1),
(11111111111116, 'Client Final 6A', 'Adresse Client Final 6A', 1),
(11111111111117, 'Client Final 7A', 'Adresse Client Final 7A', 1),
(11111111111118, 'Client Final 8A', 'Adresse Client Final 8A', 1),
-- Clients du Prestataire B
(22222222222221, 'Client Final 1B', 'Adresse Client Final 1B', 2),
(22222222222222, 'Client Final 2B', 'Adresse Client Final 2B', 2),
(22222222222223, 'Client Final 3B', 'Adresse Client Final 3B', 2),
(22222222222224, 'Client Final 4B', 'Adresse Client Final 4B', 2),
(22222222222225, 'Client Final 5B', 'Adresse Client Final 5B', 2),
(22222222222226, 'Client Final 6B', 'Adresse Client Final 6B', 2),
(22222222222227, 'Client Final 7B', 'Adresse Client Final 7B', 2),
(22222222222228, 'Client Final 8B', 'Adresse Client Final 8B', 2),
-- Clients du Prestataire C
(33333333333331, 'Client Final 1C', 'Adresse Client Final 1C', 3),
(33333333333332, 'Client Final 2C', 'Adresse Client Final 2C', 3),
(33333333333333, 'Client Final 3C', 'Adresse Client Final 3C', 3),
(33333333333334, 'Client Final 4C', 'Adresse Client Final 4C', 3),
(33333333333335, 'Client Final 5C', 'Adresse Client Final 5C', 3),
(33333333333336, 'Client Final 6C', 'Adresse Client Final 6C', 3),
(33333333333337, 'Client Final 7C', 'Adresse Client Final 7C', 3),
(33333333333338, 'Client Final 8C', 'Adresse Client Final 8C', 3),
-- Clients du Prestataire D
(44444444444441, 'Client Final 1D', 'Adresse Client Final 1D', 4),
(44444444444442, 'Client Final 2D', 'Adresse Client Final 2D', 4),
(44444444444443, 'Client Final 3D', 'Adresse Client Final 3D', 4),
(44444444444444, 'Client Final 4D', 'Adresse Client Final 4D', 4),
(44444444444445, 'Client Final 5D', 'Adresse Client Final 5D', 4),
(44444444444446, 'Client Final 6D', 'Adresse Client Final 6D', 4),
(44444444444447, 'Client Final 7D', 'Adresse Client Final 7D', 4),
(44444444444448, 'Client Final 8D', 'Adresse Client Final 8D', 4),
-- Clients du Prestataire E
(55555555555551, 'Client Final 1E', 'Adresse Client Final 1E', 5),
(55555555555552, 'Client Final 2E', 'Adresse Client Final 2E', 5),
(55555555555553, 'Client Final 3E', 'Adresse Client Final 3E', 5),
(55555555555554, 'Client Final 4E', 'Adresse Client Final 4E', 5),
(55555555555555, 'Client Final 5E', 'Adresse Client Final 5E', 5),
(55555555555556, 'Client Final 6E', 'Adresse Client Final 6E', 5),
(55555555555557, 'Client Final 7E', 'Adresse Client Final 7E', 5),
(55555555555558, 'Client Final 8E', 'Adresse Client Final 8E', 5);



INSERT INTO CONTACT (nom_contact, id_client, tel_contact, mail_contact)
VALUES
-- Contacts pour les prestataires
('Contact Prestataire A', 1, '0101234567', 'contact_prestataire_a@example.com'),
('Contact Prestataire B', 2, '0102345678', 'contact_prestataire_b@example.com'),
('Contact Prestataire C', 3, '0103456789', 'contact_prestataire_c@example.com'),
('Contact Prestataire D', 4, '0104567890', 'contact_prestataire_d@example.com'),
('Contact Prestataire E', 5, '0105678901', 'contact_prestataire_e@example.com'),
-- Contacts pour les clients du Prestataire A
('Contact 1A', 6, '0106789012', 'contact1a@example.com'),
('Contact 2A', 7, '0107890123', 'contact2a@example.com'),
('Contact 3A', 8, '0108901234', 'contact3a@example.com'),
('Contact 4A', 9, '0109012345', 'contact4a@example.com'),
('Contact 5A', 10, '0100123456', 'contact5a@example.com'),
('Contact 6A', 11, '0101234567', 'contact6a@example.com'),
('Contact 7A', 12, '0102345678', 'contact7a@example.com'),
('Contact 8A', 13, '0103456789', 'contact8a@example.com'),
-- Contacts pour les clients du Prestataire B
('Contact 1B', 14, '0104567890', 'contact1b@example.com'),
('Contact 2B', 15, '0105678901', 'contact2b@example.com'),
('Contact 3B', 16, '0106789012', 'contact3b@example.com'),
('Contact 4B', 17, '0107890123', 'contact4b@example.com'),
('Contact 5B', 18, '0108901234', 'contact5b@example.com'),
('Contact 6B', 19, '0109012345', 'contact6b@example.com'),
('Contact 7B', 20, '0100123456', 'contact7b@example.com'),
('Contact 8B', 21, '0101234567', 'contact8b@example.com'),
-- Contacts pour les clients du Prestataire C
('Contact 1C', 22, '0102345678', 'contact1c@example.com'),
('Contact 2C', 23, '0103456789', 'contact2c@example.com'),
('Contact 3C', 24, '0104567890', 'contact3c@example.com'),
('Contact 4C', 25, '0105678901', 'contact4c@example.com'),
('Contact 5C', 26, '0106789012', 'contact5c@example.com'),
('Contact 6C', 27, '0107890123', 'contact6c@example.com'),
('Contact 7C', 28, '0108901234', 'contact7c@example.com'),
('Contact 8C', 29, '0109012345', 'contact8c@example.com'),
-- Contacts pour les clients du Prestataire D
('Contact 1D', 30, '0100123456', 'contact1d@example.com'),
('Contact 2D', 31, '0101234567', 'contact2d@example.com'),
('Contact 3D', 32, '0102345678', 'contact3d@example.com'),
('Contact 4D', 33, '0103456789', 'contact4d@example.com'),
('Contact 5D', 34, '0104567890', 'contact5d@example.com'),
('Contact 6D', 35, '0105678901', 'contact6d@example.com'),
('Contact 7D', 36, '0106789012', 'contact7d@example.com'),
('Contact 8D', 37, '0107890123', 'contact8d@example.com'),
-- Contacts pour les clients du Prestataire E
('Contact 1E', 38, '0108901234', 'contact1e@example.com'),
('Contact 2E', 39, '0109012345', 'contact2e@example.com'),
('Contact 3E', 40, '0100123456', 'contact3e@example.com'),
('Contact 4E', 41, '0101234567', 'contact4e@example.com'),
('Contact 5E', 42, '0102345678', 'contact5e@example.com'),
('Contact 6E', 43, '0103456789', 'contact6e@example.com'),
('Contact 7E', 44, '0104567890', 'contact7e@example.com'),
('Contact 8E', 45, '0105678901', 'contact8e@example.com');

-- Insertion des locations de sondes
INSERT INTO LOCATION (id_client, num_sonde, date_deb_loc, date_fin_loc, date_recup_sonde) VALUES
-- Prestataire A
(6, 0000000001, '2023-06-01', '2026-06-10', NULL),
(7, 0000000002, '2023-06-03', '2023-06-08', '2023-06-08'),
-- Prestataire B
(14, 0000000003, '2021-06-01', '2023-06-05', NULL),
(15, 0000000004, '2021-06-01', '2024-06-05', NULL),
-- Prestataire C
(22, 0000000005, '2021-06-01', '2024-06-05', NULL),
(23, 0000000006, '2021-06-01', '2024-06-05', NULL),
-- Prestataire D
(30, 0000000007, '2021-06-01', '2024-06-05', NULL),
(31, 0000000008, '2021-06-01', '2024-06-05', NULL),
(32, 0000000009, '2021-06-01', '2024-06-05', NULL),
-- Prestataire E
(38, 0000000010, '2021-06-01', '2024-06-05', NULL),
(39, 0000000011, '2021-06-01', '2024-06-05', NULL),
(40, 0000000012, '2021-06-01', '2024-06-05', NULL),
(41, 0000000013, '2021-06-01', '2024-06-05', '2024-06-05')
;

-- Insertion des données pour la table LAN
INSERT INTO LAN (adresse_reseau, nom_reseau, id_client) VALUES
('192.168.1.0/24', 'LAN 1', 1),
('192.168.2.0/24', 'LAN 2', 2),
('192.168.3.0/24', 'LAN 3', 3),
('192.168.4.0/24', 'LAN 4', 4),
('192.168.5.0/24', 'LAN 5', 5);

-- Insertion des données pour la table RESEAU_SONDE
INSERT INTO RESEAU_SONDE (num_sonde, id_LAN, ip_defaut_sonde) VALUES
(1, 1, '192.168.1.10'),
(2, 2, '192.168.2.20'),
(3, 3, '192.168.3.30'),
(4, 4, '192.168.4.40'),
(5, 5, '192.168.5.50');

-- Insertion des données pour la table COMMUNICATION_VPN
INSERT INTO COMMUNICATION_VPN (num_sonde, nom_tunnel, adresse_client_VPN) VALUES
(1, 'Tunnel 1', '10.0.0.1'),
(2, 'Tunnel 2', '10.0.0.2'),
(3, 'Tunnel 3', '10.0.0.3'),
(4, 'Tunnel 4', '10.0.0.4'),
(5, 'Tunnel 5', '10.0.0.5');

-- Insertion des données pour la table OPERATION_SONDE
INSERT INTO OPERATION_SONDE (num_sonde, motif_operation, type_operation, id_employe, date_recup_sonde, debut_operation, fin_operation) VALUES
(0000000001, 'Dépannage ponctuel', 'Redémarrage', 1, NULL, '2023-06-01 10:00:00', NULL),
(0000000002, 'Audit', 'Déploiement', 2, '2023-06-02', '2023-06-02 08:00:00', '2023-06-02 12:00:00'),
(0000000003, 'Maintenance', 'Déploiement', 3, NULL, '2023-06-03 15:00:00', NULL),
(0000000004, 'Maintenance', 'Redémarrage', 4, '2023-06-04', '2023-06-04 09:00:00', '2023-06-04 10:00:00'),
(0000000005, 'Dépannage ponctuel', 'Redémarrage', 5, NULL, '2023-06-05 14:00:00', NULL);

-- Ajout de la durée de l'opération de maintenance (colonne duree_operation) dans la table OPERATION_SONDE : si la date de fin d'opération est précisée, on fait la différence entre la date de fin et la date de début, sinon on fait la différence avec la date du jour.
ALTER TABLE OPERATION_SONDE ADD duree_operation INT AFTER fin_operation;
UPDATE OPERATION_SONDE
SET duree_operation = IF(fin_operation IS NULL, TIMESTAMPDIFF(DAY, debut_operation, NOW()), TIMESTAMPDIFF(DAY, debut_operation, fin_operation));

INSERT INTO INCIDENT (num_sonde, date_incident, id_operation) VALUES
(1, '2023-06-01 11:00:00', 1),
(2, '2023-06-02 09:00:00', 2),
(2, '2023-06-03 09:00:00', NULL);

-- Insertion des employés
INSERT INTO EMPLOYE (nom_employe, prenom_employe) VALUES
('Employe 1', 'Prenom 1'),
('Employe 2', 'Prenom 2'),
('Employe 3', 'Prenom 3'),
('Employe 4', 'Prenom 4'),
('Employe 5', 'Prenom 5'),
('Employe 6', 'Prenom 6'),
('Employe 7', 'Prenom 7'),
('Employe 8', 'Prenom 8'),
('Employe 9', 'Prenom 9'),
('Employe 10', 'Prenom 10');


-- Contraintes FK : placées en fin de script de sorte à pouvoir créer les tables/faire l'insertion des données dans n'importe quel ordre.

ALTER TABLE SONDE ADD CONSTRAINT CK_FK_SONDE_semaos FOREIGN KEY (id_semaos) REFERENCES SEMAOS(id_semaos);
ALTER TABLE DISQUE_SEMA ADD CONSTRAINT CK_FK_DISQUE_SEMA_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE SCRIPT_SONDE ADD CONSTRAINT CK_FK_SCRIPT_SONDE_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE SCRIPT_SONDE ADD CONSTRAINT CK_FK_SCRIPT_SONDE_SCRIPT FOREIGN KEY (id_script) REFERENCES SCRIPT(id_script) ON DELETE CASCADE;
ALTER TABLE CONTACT ADD CONSTRAINT CK_FK_CONTACT_CLIENT FOREIGN KEY (id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE;
ALTER TABLE LOCATION ADD CONSTRAINT CK_FK_LOCATION_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde);
ALTER TABLE LOCATION ADD CONSTRAINT CK_FK_LOCATION_CLIENT FOREIGN KEY (id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE; 
ALTER TABLE LAN ADD CONSTRAINT CK_FK_LAN_CLIENT FOREIGN KEY(id_client) REFERENCES CLIENT(id_client) ON DELETE CASCADE;
ALTER TABLE RESEAU_SONDE ADD CONSTRAINT FK_RESEAU_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE RESEAU_SONDE ADD CONSTRAINT CK_FK_RESEAU_SONDE_RESEAU FOREIGN KEY (id_LAN) REFERENCES LAN(id_LAN) ON DELETE CASCADE;
ALTER TABLE COMMUNICATION_VPN ADD CONSTRAINT CK_FK_COMMUNICATION_VPN_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE OPERATION_SONDE ADD CONSTRAINT CK_FK_OPERATION_SONDE_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE OPERATION_SONDE ADD CONSTRAINT CK_FK_OPERATION_SONDE_EMPLOYE FOREIGN KEY (id_employe) REFERENCES EMPLOYE(id_employe);
ALTER TABLE INCIDENT ADD CONSTRAINT CK_FK_INCIDENT_SONDE FOREIGN KEY (num_sonde) REFERENCES SONDE(num_sonde) ON DELETE CASCADE;
ALTER TABLE INCIDENT ADD CONSTRAINT CK_FK_INCIDENT_OPERATION FOREIGN KEY (id_operation) REFERENCES OPERATION_SONDE(id_operation) ON DELETE SET NULL;


-- Contrainte sur l'association réflexive de la table CLIENT : placée après l'insertion des données afin de pouvoir ajouter des clients finaux ou des prestataires dans n'importe quel ordre.
-- Renvoie une erreur si le SIRET n'est pas composé de 14 chiffres et si la valeur de la colonne prestataire ne renvoie pas à une entrée de la table CLIENT

ALTER TABLE CLIENT ADD CONSTRAINT CK_FK_CLIENT_prestataire FOREIGN KEY (prestataire) REFERENCES CLIENT(id_client);


-- Index
CREATE INDEX IDX_SONDE_nom ON SONDE (nom_sonde);

EOF
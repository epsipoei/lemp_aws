#!/usr/bin/python3

import os
import subprocess
import requests

# Function to retrieve the public IP address
def get_public_ip():
    response = requests.get('https://api.ipify.org').text
    return response

def get_server_public_key():
    # Exécuter la commande `wg show wg0` pour obtenir les informations du serveur VPN
    result = subprocess.run(["wg", "show", "wg0"], capture_output=True, encoding="utf-8")
    output = result.stdout

    # Extraire la clé publique du serveur à partir de la sortie de la commande
    server_public_key = output.split("\n")[1].split(":")[1].strip()

    return server_public_key

def title():
    title = "WireGuard Client Generator"
    line = "-" * len(title)

    print("\033[1;36m" + line)
    print(title)
    print(line + "\033[0m")

def generate_wireguard_client():
    
    title()
    print("\n\033[1;33mThis Python program generates WireGuard clients for connecting to a WireGuard VPN server.\033[0m\n")
    
    while True:
        ip_pub = get_public_ip()
        # Demander les informations nécessaires
        client_name = input("Nom du client : ")
        private_ip = input("Adresse IP privée à utiliser : ")
        public_ip = input(f"Adresse IP publique du serveur VPN (local: {ip_pub}): ")
        if not public_ip:
            public_ip = ip_pub

        subnet_aws = input("Subnet aws (x.x.x.x/x): ") 
        
        # Générer les clés du client
        client_private_key = subprocess.run(["wg", "genkey"], capture_output=True, encoding="utf-8").stdout.strip()
        client_public_key = subprocess.run(["wg", "pubkey"], input=client_private_key, capture_output=True,
                                            encoding="utf-8").stdout.strip()

        # Récupérer la clé publique du serveur VPN
        server_public_key = get_server_public_key()

        # Ajouter le client au fichier de configuration wg0
        wg0_conf = f"""
[Peer]
PublicKey = {client_public_key}
AllowedIPs = {private_ip}/32
"""

        with open("/etc/wireguard/wg0.conf", "a") as f:
            f.write(wg0_conf)

        # Créer le fichier de configuration du client
        client_conf = f"""
[Interface]
PrivateKey = {client_private_key}
Address = {private_ip}/32
DNS = 1.1.1.1

[Peer]
PublicKey = {server_public_key}
AllowedIPs = 10.10.10.0/24, {subnet_aws}
Endpoint = {public_ip}:13579
"""

        with open(f"{client_name}_wg.conf", "w") as f:
            f.write(client_conf)

        print("\033[1;32mLe client WireGuard a été généré avec succès !\033[0m")
        print(f"\n\033[1;33mYour {client_name}_wg.conf, you can copy this file in your wireguard for run the vpn\033[0m")
        os.system(f"cat {client_name}_wg.conf")

        # Demander si l'utilisateur souhaite quitter
        exit_choice = input("\n\033[1;36mDo you want exit WireGuard Client Generator\033[0m ? (y/n): ")
        if exit_choice.lower() == "y":
            break

generate_wireguard_client()

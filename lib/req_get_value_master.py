#!/usr/bin/python3

import re
import fileinput
import pymysql

# Connexion à la base de données maître
db_master = pymysql.connect(
    host="10.10.10.3", user="admin",port=6033, password="iopiop", database="mspr"
)

# Création du curseur
cursor = db_master.cursor()

# Exécution de la requête pour obtenir les valeurs de file et position
query = "SHOW MASTER STATUS"
cursor.execute(query)

# Récupération des résultats
result = cursor.fetchone()
file_value = result[0]
position_value = result[1]

# Fermeture de la connexion
db_master.close()

# Remplacement des valeurs dans le fichier mysql.sh
file_path = "Ansible/roles/mysql_slave/files/mysql.sh"

# Remplacement de la valeur de "filename"
with fileinput.FileInput(file_path, inplace=True) as file:
    for line in file:
        line = re.sub(r'filename=".*?"', f'filename="{file_value}"', line)
        print(line, end="")

# Remplacement de la valeur de "log_pos"
with fileinput.FileInput(file_path, inplace=True) as file:
    for line in file:
        line = re.sub(r"log_pos=.*", f"log_pos={position_value}", line)
        print(line, end="")

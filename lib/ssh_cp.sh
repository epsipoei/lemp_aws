#!/bin/bash

mysql_ip_pub="13.48.42.39"
backup_1_ip_priv="172.31.38.38"
backup_1_ip_pub="16.170.240.18"
backup_2_ip_priv="172.31.32.175"

ssh -t ubuntu@$mysql_ip_pub "sudo -u backy ssh-copy-id -i /home/backy/.ssh/id_ed25519.pub backy@$backup_1_ip_priv"
ssh -t ubuntu@$backup_1_ip_pub "sudo -u backy ssh-copy-id -i /home/backy/.ssh/id_ed25519.pub backy@$backup_2_ip_priv"
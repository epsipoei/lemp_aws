# Handle updating IP addresses for MySQL
update_mysql_ips() {
  local choice
  new_ip_pub="None"

  clear
  print_title
  echo -e "\n${YELLOW}Get your public IP address from https://aws.amazon.com/fr/${NC}"
  echo -e "\nChoice :"
  echo "1) Public_IP "
  echo "2) Private_IP"
  echo -e "${BLUE}3) Return${NC}"
  printf "\nYour choice: "
  read choice
  case $choice in
    1|Public_IP)
      clear
      print_title
      echo -e "\n${YELLOW}Get your public IP address from https://aws.amazon.com/fr/${NC}"
      echo -e "\n${GREEN}Choose your command:${NC}\n"
      PS3="Your choice: "
      select choice in \
      "Mysql_Master" \
      "Mysql_Slave" \
      "HA_Proxy" \
      "Zabbix" \
      "My_vpn" \
      "Backup_1" \
      "Backup_2" \
      "Return"; do
        if [ "$choice" == "Return" ]; then
          clear
          break
        elif [ "$choice" == "Mysql_Master" ]; then
          old_ip_pub=$(grep "mysql_master ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_mysql_master/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/mysql_master ansible_host=$old_ip_pub/mysql_master ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
          sed -i "s/= \"$old_ip_pub\"/= \"$new_ip_pub\"/g" Ansible/roles/nginx/files/php/sonde.php
          sed -i "s/server_name $old_ip_pub/server_name $new_ip_pub/g" Ansible/roles/nginx/files/cma4.conf
          sed -i "s/host=\"$old_ip_pub\"/host=\"$new_ip_pub\"/g" lib/req_get_value_master.py
          sed -i "s/mysql_ip_pub=\"$old_ip_pub\"/mysql_ip_pub=\"$new_ip_pub\"/g" lib/ssh_cp.sh

        elif [ "$choice" == "Mysql_Slave" ]; then
          old_ip_pub=$(grep "mysql_slave ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_mysql_slave/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/mysql_slave ansible_host=$old_ip_pub/mysql_slave ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

        elif [ "$choice" == "HA_Proxy" ]; then
          old_ip_pub=$(grep "haproxy_mysql ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_proxy_mysql/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/haproxy_mysql ansible_host=$old_ip_pub/haproxy_mysql ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

        elif [ "$choice" == "Zabbix" ]; then
          old_ip_pub=$(grep "srv_zabbix ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_srv_zabbix/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/srv_zabbix ansible_host=$old_ip_pub/srv_zabbix ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

        elif [ "$choice" == "My_vpn" ]; then
          old_ip_pub=$(grep "my_vpn ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_my_vpn/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/my_vpn ansible_host=$old_ip_pub/my_vpn ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
          sed -i "s/ssh -t ubuntu@$old_ip_pub/ssh -t ubuntu@$new_ip_pub/g" lib/menu.sh
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/backup_1/files/wg-mspr.conf
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/backup_2/files/wg-mspr.conf
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/mysql/files/wg-mspr.conf
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/mysql_slave/files/wg-mspr.conf
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/proxy_mysql/files/wg-mspr.conf
          sed -i "s/Endpoint = $old_ip_pub:13579/Endpoint = $new_ip_pub:13579/g" Ansible/roles/zabbix/files/env/wg-mspr.conf

        elif [ "$choice" == "Backup_1" ]; then
          old_ip_pub=$(grep "backup_1 ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_backup_1/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/backup_1 ansible_host=$old_ip_pub/backup_1 ansible_host=$new_ip_pub/g" Ansible/inventory/hosts
          sed -i "s/REMOTE_SERVER=\"$old_ip_pub\"/REMOTE_SERVER=\"$new_ip_pub\"/g" Ansible/roles/mysql/files/backup.sh
          sed -i "s/backup_1_ip_pub=\"$old_ip_pub\"/backup_1_ip_pub=\"$new_ip_pub\"/g" lib/ssh_cp.sh

        elif [ "$choice" == "Backup_2" ]; then
          old_ip_pub=$(grep "backup_2 ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
          new_ip_pub=$(grep '"public_ip":' Terraform/prj_backup_2/terraform.tfstate | awk -F '"public_ip": "|"' '{print $2}')
          sed -i "s/backup_2 ansible_host=$old_ip_pub/backup_2 ansible_host=$new_ip_pub/g" Ansible/inventory/hosts

        else
          echo -e "Invalid command."
        fi
      echo -e "\n${YELLOW}Your new_ip_pub: $new_ip_pub${NC}"  
      echo -e "${GREEN}Successfull${NC}"
      done 
      ;;
    2|Private_IP)
      clear
      print_title
      echo -e "\n${YELLOW}Get your public IP address from https://aws.amazon.com/fr/${NC}"
      echo -e "\n${GREEN}Choose your command:${NC}\n"
      PS3="Your choice:"
      select choice in \
      "Mysql_Master" \
      "Mysql_Slave" \
      "HA_Proxy" \
      "Zabbix" \
      "My_vpn" \
      "Backup_1" \
      "Backup_2" \
      "Return"; do
        if [ "$choice" == "Return" ]; then
          clear
          break
        
        elif [ "$choice" == "Mysql_Master" ]; then
          old_ip_priv=$(grep 'prj_mysql_master =' Terraform/hosts | awk -F'prj_mysql_master = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_mysql_master/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/server mysql1 $old_ip_priv:6033 check/server mysql1 $new_ip_priv:6033 check/g" Ansible/roles/proxy_mysql/files/haproxy.cfg
          sed -i "s/master_ip=\"$old_ip_priv\"/master_ip=\"$new_ip_priv\"/g" Ansible/roles/mysql_slave/files/mysql.sh
          sed -i "s/prj_mysql_master = $old_ip_priv/prj_mysql_master = $new_ip_priv/g" Terraform/hosts

        elif [ "$choice" == "Mysql_Slave" ]; then
          old_ip_priv=$(grep 'prj_mysql_slave =' Terraform/hosts | awk -F'prj_mysql_slave = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_mysql_slave/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/prj_mysql_slave = $old_ip_priv/prj_mysql_slave = $new_ip_priv/g" Terraform/hosts
          sed -i "s/mysql_slave ansible_host=$old_ip_priv/mysql_slave ansible_host=$new_ip_priv/g" Ansible/inventory/hosts
          sed -i "s/server mysql2 $old_ip_priv:6033 check/server mysql2 $new_ip_priv:6033 check/g" Ansible/roles/proxy_mysql/files/haproxy.cfg
        
        elif [ "$choice" == "HA_Proxy" ]; then
          old_ip_priv=$(grep 'prj_proxy_mysql =' Terraform/hosts | awk -F'prj_proxy_mysql = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_proxy_mysql/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/prj_proxy_mysql = $old_ip_priv/prj_proxy_mysql = $new_ip_priv/g" Terraform/hosts

        elif [ "$choice" == "Zabbix" ]; then
          old_ip_priv=$(grep 'prj_srv_zabbix =' Terraform/hosts | awk -F'prj_srv_zabbix = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_srv_zabbix/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/prj_srv_zabbix = $old_ip_priv/prj_srv_zabbix = $new_ip_priv/g" Terraform/hosts
          sed -i "s/prj_srv_zabbix = $old_ip_priv/prj_srv_zabbix = $new_ip_priv/g" Terraform/hosts

        elif [ "$choice" == "My_vpn" ]; then
          old_ip_priv=$(grep 'prj_my_vpn =' Terraform/hosts | awk -F'prj_my_vpn = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_my_vpn/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/prj_my_vpn = $old_ip_priv/prj_my_vpn = $new_ip_priv/g" Terraform/hosts

        elif [ "$choice" == "Backup_1" ]; then
          old_ip_priv=$(grep 'prj_backup_1 =' Terraform/hosts | awk -F'prj_backup_1 = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_backup_1/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/hosts allow = $old_ip_priv/hosts allow = $new_ip_priv/g" Ansible/roles/backup_2/files/rsyncd.conf
          sed -i "s/REMOTE_SERVER=\"$old_ip_priv\"/REMOTE_SERVER=\"$new_ip_priv\"/g" Ansible/roles/mysql/files/backup.sh
          sed -i "s/backup_1_ip_priv=\"$old_ip_priv\"/backup_1_ip_priv=\"$new_ip_priv\"/g" lib/ssh_cp.sh
          sed -i "s/prj_backup_1 = $old_ip_priv/prj_backup_1 = $new_ip_priv/g" Terraform/hosts

        elif [ "$choice" == "Backup_2" ]; then
          old_ip_priv=$(grep 'prj_backup_2 =' Terraform/hosts | awk -F'prj_backup_2 = ' '{print $2}')
          new_ip_priv=$(grep '"private_ip":' Terraform/prj_backup_2/terraform.tfstate | awk -F '"private_ip": "|"' '{print $2}')
          sed -i "s/hosts allow = $old_ip_priv/hosts allow = $new_ip_priv/g" Ansible/roles/backup_1/files/rsyncd.conf
          sed -i "s/prj_backup_2 = $old_ip_priv/prj_backup_2 = $new_ip_priv/g" Terraform/hosts
          sed -i "s/backup_2_ip_priv=\"$old_ip_priv\"/backup_2_ip_priv=\"$new_ip_priv\"/g" lib/ssh_cp.sh
          sed -i "s/backup_2_ip_priv=\"$old_ip_priv\"/backup_2_ip_priv=\"$new_ip_priv\"/g" Ansible/roles/backup_1/files/rsync.sh

        else
          echo -e "Invalid command."
        fi
      echo -e "\n${YELLOW}Your new_ip_priv: $new_ip_priv${NC}"  
      echo -e "${GREEN}Successfull${NC}"
      done
      ;;
    3|Return)
      clear
      ;;
    *)
      echo -e "Invalid choice."
      ;;
  esac
}
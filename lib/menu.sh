# Display the main menu
print_main_menu() {
  echo -e "\nChoice :\n"
  echo "1) Terraform"
  echo "2) Ansible"
  echo "3) Update_ip"
  echo "4) Manage_my_vpn"
  echo -e "${CYAN}5) Exit${NC}"
}

# Dixplay my_vpn menu
print_my_vpn_menu() {
  ssh -t ubuntu@51.20.55.174 'sudo /usr/local/bin/my_vpn.sh'
}

# Display the Terraform menu
print_terraform_menu() {
  echo -e "\n${CYAN}Choose a project:${NC}\n"
  PS3="Input your choice: "
  select project in "${projects[@]}"; do
    if [ "$project" == "Return" ]; then
      clear
      break
    elif [[ -n $project ]]; then
      clear
      print_title
      while true; do
        echo -e "\n${CYAN}Choose your command:${NC}\n"
        PS3="Input your choice: "
        select command in "init" "plan" "apply" "destroy" "Return"; do
          if [[ -n $command ]]; then
            if [ "$command" == "Return" ]; then
              clear
              break 3
            else
              cd "Terraform/$project/" && terraform "$command"
              cd ../..
              break
            fi
          else
            echo -e "Invalid command."
          fi
        done
      done
    else
      echo -e "Invalid project."
    fi
  done
}


# Display the Ansible menu
print_ansible_menu() {
  while true; do
    echo -e "\n${YELLOW}Get your public IP address from https://aws.amazon.com/fr/${NC}"
    echo -e "\nChoice :"
    echo "1) Exec_playbook"
    echo "2) Exec_scp_dump"
    echo "3) Activate_Backup_Mysql"
    echo -e "${CYAN}4) Return${NC}"
    printf "\nInput your choice: "
    read choice
    case $choice in
      1|Exec_playbook)
        clear
        print_title
        printf "\n${GREEN}Choose a project:${NC}\n\n"
        PS3="Input your choice: "
        select project in "${projects[@]}"; do
          if [ "$project" == "Return" ]; then
            clear
            break 3
          elif [[ -n $project ]]; then
            if [ "$project" != "prj_mysql_slave" ]; then
              ansible-playbook -i Ansible/inventory/ Ansible/playbook/deploy_$project.yml
              break
            else
              python3 lib/req_get_value_master.py
              ansible-playbook -i Ansible/inventory/ Ansible/playbook/deploy_$project.yml
              break
            fi 
          else
            echo -e "Invalid project."
          fi
        done
        ;;
      2|Exec_scp_dump)
        master_ip_pub=$(grep "mysql_master ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
        scp ubuntu@$master_ip_pub:~/dump.sql  /tmp/dump.sql
        slave_ip_pub=$(grep "mysql_slave ansible_host=" Ansible/inventory/hosts | awk -F"ansible_host=| ansible_user" '{print $2}')
        scp /tmp/dump.sql ubuntu@$slave_ip_pub:/tmp
        ;;
      3|Activate_Backup_Mysql)
        echo -e "\n${YELLOW}For activate backup_mysql you must input user_backy password !${NC}\n"
        bash lib/ssh_cp.sh
        ;;
      4|Return)
        clear
        break 3
        ;;
      *)
        echo -e "Invalid choice."
        ;;
    esac
  done
}
# Display the program title
print_title() {
  local title_length=${#TITLE}
  local frame_width=$((title_length + 4))
  local border=$(printf '%*s' "$frame_width" | tr ' ' '=')

  echo -e "${MAGENTA}$border${NC}"
  echo -e "${MAGENTA}|${NC} ${CYAN}$TITLE${NC} ${MAGENTA}|${NC}"
  echo -e "${MAGENTA}$border${NC}"
  echo
}

# Display AWS instructions
print_instructions() {
  local instruction="${YELLOW}AWS instruction, you can create or edit EC2 with Terraform or update configuration with Ansible:${NC}"
  echo -e "${instruction}"
}



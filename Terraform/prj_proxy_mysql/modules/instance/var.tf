variable "vpc_id" {
  description = "ID de la VPC TF_VAR_vpc_id"
  default     = ""
  type        = string
}

variable "ports_tcp" {
  default = { ssh = "22", https = "443", http = "80", mysql = "6033", zabbix = "10050", my_vpn = "13579" }
  type    = map(string)
}

variable "ports_udp" {
  default = { my_vpn = "13579" }
  type    = map(string)
}

variable "ami_id" {
  default = "ami-0989fb15ce71ba39e"
  type    = string
}

variable "AWS_ACCESS_KEY" {
  type        = string
  description = "AWS access key. Can also be set via the environment variable AWS_ACCESS_KEY_ID."
  default     = ""
  sensitive   = true
}

variable "AWS_SECRET_KEY" {
  type        = string
  description = "AWS secret key. Can also be set via the environment variable AWS_SECRET_ACCESS_KEY."
  default     = ""
  sensitive   = true
}

variable "private_ip" {
  type    = string
  default = "172.31.16.52" #ip_private 172.31.16.52
}

resource "aws_instance" "proxy_mysql" {
  ami           = var.ami_id
  instance_type = "t3.micro"
  root_block_device {
    volume_size           = 8     # Taille du volume en Go
    volume_type           = "gp2" # Type de volume EBS
    delete_on_termination = true  # Supprimer le volume lors de la suppression de l'instance
  }
  tags = {
    "Name" = "proxy_mysql"
  }
  security_groups = [aws_security_group.proxy_mysql.name]
  key_name        = "key_ssh"
}

resource "aws_security_group" "proxy_mysql" {
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "all_in_tcp" {
  for_each          = var.ports_tcp
  type              = "ingress"
  from_port         = each.value
  to_port           = each.value
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.proxy_mysql.id
}

resource "aws_security_group_rule" "all_in_udp" {
  for_each          = var.ports_udp
  type              = "ingress"
  from_port         = each.value
  to_port           = each.value
  protocol          = "udp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.proxy_mysql.id
}

resource "aws_security_group_rule" "icmp_in" {
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.proxy_mysql.id
}

resource "aws_security_group_rule" "all_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.proxy_mysql.id
}

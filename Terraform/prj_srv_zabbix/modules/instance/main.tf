resource "aws_instance" "srv_zabbix" {
  ami           = var.ami_id
  instance_type = "t3.small"
  tags = {
    "Name" = "srv_zabbix"
  }
  security_groups = [aws_security_group.srv_zabbix.name]
  key_name        = "key_ssh"
}

resource "aws_security_group" "srv_zabbix" {
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "all_in_tcp" {
  for_each          = var.ports_tcp
  type              = "ingress"
  from_port         = each.value
  to_port           = each.value
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.srv_zabbix.id
}

resource "aws_security_group_rule" "all_in_udp" {
  for_each          = var.ports_udp
  type              = "ingress"
  from_port         = each.value
  to_port           = each.value
  protocol          = "udp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.srv_zabbix.id
}

resource "aws_security_group_rule" "icmp_in" {
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.srv_zabbix.id
}

resource "aws_security_group_rule" "all_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.srv_zabbix.id
}

.. ec2_management_tool documentation master file, created by
   sphinx-quickstart on Tue Aug 29 16:13:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

"EC2 Management Tool's" documentation !
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


**1. Introduction**
-------------------

*Tool Objective*

The EC2 Instance Management Tool, also known as the "EC2 Management Tool," is designed to simplify and automate the management of EC2 instances on AWS. It offers a user-friendly interface that allows administrators to deploy and manage infrastructure, execute tasks using Ansible, and automate IP address updates.
Context

This tool was developed in the context of Cloud infrastructure management, where the need to effectively interact with EC2 instances, update IP addresses, and manage VPN settings was crucial.
Benefits

- Centralized Management: The tool provides a centralized interface to interact with EC2 instances, eliminating the need to juggle between different tools and terminals.
- Time Savings: Automated features such as infrastructure deployment, IP address updates, and task execution reduce the time required to perform these operations.
- Enhanced Organization: Menus and sub-menus facilitate navigation and execution of specific tasks, reinforcing the work structure.
- Task Automation: The tool automates tasks such as IP address updates and VPN settings management, eliminating manual and tedious steps.

**2. Prerequisites**
--------------------

*Dependencies*

The tool relies on the following components:

- The script lib/title.sh: Contains the print_title function that displays the program's title.
- The script lib/menu.sh: Contains various menu-printing functions used in the script.
- The script lib/handle_updating_ip.sh: Contains the update_mysql_ips function to manage IP address updates.

*Initial Configuration*

Before using the tool, ensure that you have:

- Configured your AWS credentials and securely stored them.
- Placed all necessary scripts and files in the appropriate directory.

**3. Key Features**
-------------------

*Main Menu*

The main menu of the tool offers the following options:

- Management with Terraform: Allows interaction with Terraform to deploy, plan, apply, or destroy infrastructure projects.
- Management with Ansible: Provides choices to execute playbooks, copy files, and enable backup settings for MySQL.
- IP Address Updates: Automates IP address updates across different roles.
- VPN Settings Management: Allows management of VPN settings by running the my_vpn.sh script.

**4. Tool Usage**
-----------------

*Script Execution*

- Ensure you have fulfilled the prerequisites.
- Execute the main script to access the menu.

*Navigating the Menus*

- Choose the appropriate option in the main menu.
- Follow sub-menus and make choices based on your needs.

*Using Terraform*

- The Terraform sub-menu enables initializing, planning, applying, or destroying infrastructure projects.

*Using Ansible*

- The Ansible sub-menu offers options to execute playbooks, copy files, and enable backup settings for MySQL.

*IP Address Updates*

- The IP address update sub-menu automatically updates IP addresses across different roles.

*VPN Settings Management*

- The VPN settings management sub-menu runs the my_vpn.sh script to manage VPN settings.

**5. Benefits**
---------------

- Centralized Management: The tool provides a unified way to manage EC2 instances and automate various tasks.
- Time Savings: Automation of common tasks reduces the time and effort required to manage infrastructure.
- Enhanced Organization: Clear and structured menus enhance visibility and understanding.
- Task Automation: The tool automates IP address updates and other tasks, reducing human errors.

**6. Security and Precautions**
-------------------------------

- Secure Management of AWS Credentials: Ensure secure storage of AWS credentials used by the tool.
- Access Control: Limit access to the tool and AWS resources to authorized personnel.

**7. Conclusion**
-----------------

*Summary of Capabilities*

The "EC2 Management Tool" simplifies EC2 instance management by offering an intuitive interface and automated features. It streamlines the infrastructure management process, improves efficiency, and contributes to security.
Importance in Cloud Resource Management

In an ever-evolving Cloud environment, this tool provides an effective solution for managing EC2 instances, ensuring operational consistency and efficiency.
Improvement Perspectives

The tool could be extended to support other features such as security group management, EBS volume management, and more.
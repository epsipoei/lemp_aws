**Role backup_slave**
=====================

**Package and System Update**

.. code-block:: yaml

   -  name: Mettre à jour les paquets
      apt:
         update_cache: yes
         upgrade: yes
         state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module.
It ensures that the server is up to date with the latest package versions.

**Install Backup Packages**

.. code-block:: yaml

   -  name: Install Backup packages
      package:
         name: "{{ item }}"
      loop:
         - rsync
         - samba

*Description:* This task installs the necessary backup-related packages, namely "rsync" and "samba," using the package module.
The loop construct allows installing multiple packages in a single task.

**Group and User Creation**

.. code-block:: yaml

   -  name: Create a new group
      group:
         name: back
         state: present

   -  name: Add user to 'back' group
      user:
         name: backy
         state: present
         groups: back
         password: "{{ 'iopiop' | password_hash('sha512', 'mysecretsalt') }}"
         shell: /bin/bash

*Description:* These tasks create a new group named "back" and add a user named "backy" to the group.
The user's password is hashed using SHA-512. This ensures that the necessary user and group for backup purposes are in place.

**SSH and Configuration Management**

.. code-block:: yaml

   -  name: Copy conf ssh
      copy:
         src: ../roles/backup_1/files/sshd_config
         dest: /etc/ssh/sshd_config
         mode: '0644'
      notify:
         - Redémarrer le service SSH

   -  meta: flush_handlers

*Description:* This task copies the SSH configuration file sshd_config from a specified source to the /etc/ssh/ directory. 
The notify directive triggers a handler to restart the SSH service. The meta command flushes any pending handlers.

**File and Permission Management**

.. code-block:: yaml

   -  name: Create share directories
      file:
         path: /srv/backup
         state: directory
         owner: backy
         group: back
         mode: '0755'

*Description:* This task creates a directory /srv/backup with specific ownership and permissions. 
This directory will be used for sharing backup data.

**Samba Configuration**

.. code-block:: yaml

   -  name: Set Samba password for user
      command: smbpasswd -a backy
      args:
         stdin: "iopiop\niopiop\n"
      become: true

   -  name: Copy smb.conf file
      copy:
         src: ../roles/backup_1/files/smb-conf
         dest: /etc/samba/smb.conf

*Description:* These tasks set the Samba password for the "backy" user and copy the Samba configuration file smb-conf to /etc/samba/.


**Role vpn**
============

**Package and System Update**

.. code-block:: yaml

   - name: Mettre à jour les paquets
     apt:
       update_cache: yes
       upgrade: yes
       state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module. 
It ensures that the server is up to date with the latest package versions.

**Install WireGuard and Tools**

.. code-block:: yaml

   - name: Install WireGuard
     apt:
       name: wireguard
       state: present

   - name: Install WireGuard Tools
     apt:
       name: wireguard-tools
       state: present

*Description:* These tasks install the WireGuard package and its associated tools using the apt module. 
It ensures that WireGuard is properly installed on the server, including the necessary command-line utilities.

**Generate Server Keys and Configuration**

.. code-block:: yaml

   - name: Generate server private key
     command: wg genkey
     register: server_private_key
     changed_when: false

   - name: Generate server public key
     command: echo "{{ server_private_key.stdout }}" | wg pubkey
     register: server_public_key
     changed_when: false

   - name: Copy WireGuard configuration file
     template:
       src: templates/wg0.conf.j2
       dest: /etc/wireguard/wg0.conf
       owner: root
       group: root
       mode: 0600
     vars:
       private_key: "{{ server_private_key.stdout }}"
       server_address_priv: "10.0.0.1"  # Replace with the VPN server's private IP address
       listen_port: 13579  # Replace with the desired listening port

*Description:* These tasks generate the server's private and public keys, then copy a WireGuard configuration template to /etc/wireguard/wg0.conf. 
The template includes the private key, server's private IP address, and the listening port. It ensures secure communication and proper configuration of the WireGuard VPN.

**Enable WireGuard Service**

.. code-block:: yaml

   - name: Enable WireGuard service
     service:
       name: wg-quick@wg0
       state: started
       enabled: true

*Description:* This task enables and starts the WireGuard service using the service module. 
It ensures that the WireGuard VPN service is up and running, and that it is set to start automatically upon system boot.

**Copy Custom Script**

.. code-block:: yaml

   - name: Copy my_vpn.py
     copy:
       src: ../roles/my_vpn/files/my_vpn.py
       dest: /usr/local/bin/my_vpn.sh
       mode: 'u+x'

*Description:* This task copies a custom script named my_vpn.py to /usr/local/bin/my_vpn.sh. 
The script can be used to manage the WireGuard VPN server. It is set to be executable (u+x) to ensure ease of use.

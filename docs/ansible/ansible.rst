**Ansible**
===========

Backup
------

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   backup_master
   backup_slave

MySQL
-----

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   mysql_master
   mysql_slave
   proxy_mysql

Vpn
---

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   vpn

Zabbix
------

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   zabbix
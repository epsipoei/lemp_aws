**Role mysql_slave**
====================

**Package and System Update**

.. code-block:: yaml

   - name: Mettre à jour les paquets
     apt:
       update_cache: yes
       upgrade: yes
       state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module. 
It ensures that the server is up to date with the latest package versions.

**Install MySQL Packages**

.. code-block:: yaml

   - name: Install MySQL packages
     package:
       name: "{{ item }}"
     loop:
       - mysql-server

*Description:* This task installs MySQL server package using the package module. 
It ensures that MySQL is properly installed on the server.

**MySQL Configuration and Replication Setup**

.. code-block:: yaml

   - name: Copy conf mysql_slave
     copy:
       src: ../roles/mysql_slave/files/my.cnf
       dest: /etc/mysql/my.cnf
       mode: '0644'

   - name: Configuration de la réplication sur le serveur esclave
     copy:
       src: ../roles/mysql_slave/files/mysql.sh
       dest: /usr/local/bin/mysql.sh
       mode: 'u+x'

*Description:* These tasks copy the MySQL configuration file my.cnf for the slave server and the replication configuration script mysql.sh to /usr/local/bin/. 
The script sets up replication settings for the slave.

**Execute MySQL Script**

.. code-block:: yaml

   - name: Execute MySQL script
     shell: sudo su -c "/usr/local/bin/mysql.sh" root
     become: true

*Description:* This task executes the mysql.sh script using the shell module. 
The script configures replication settings on the slave server. The become directive ensures the script is executed with root privileges.

**Restart MySQL**

.. code-block:: yaml

   - name: Restart MySQL
     service:
       name: mysql
       state: restarted
     become: true

*Description:* This task restarts the MySQL service to apply the replication configuration changes. 
The become directive ensures that the service is restarted with root privileges.

**Restore MySQL Backup**

.. code-block:: yaml

   - name: Restor MySQL backup
     become: true
     shell: sudo mysql < /tmp/dump.sql
     args:
       executable: /bin/bash

*Description:* This task restores a MySQL backup stored in the dump.sql file using the mysql command. 
The backup data is imported into the MySQL server to initialize replication from the master.

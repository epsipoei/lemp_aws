**var.tf**
==========

**VPC ID Variable**

.. code-block:: terraform

    variable "vpc_id" {
        description = "ID of the VPC TF_VAR_vpc_id"
        default     = ""
        type        = string
    }

*Description:* This variable is used to specify the ID of the Virtual Private Cloud (VPC) in which the resources will be deployed.
It serves as the networking foundation for your deployment.

*Default Value:* An empty string indicates that the variable must be explicitly provided during deployment.

**Ports Variable**

.. code-block:: terraform

    variable "ports_tcp" {
        default = { ssh = "22", https = "443", http = "80", mysql = "6033", zabbix_agent = "10050", zabbix_srv = "10051", zabbix_front = "8080" }
        type    = map(string)
    }

*Description:* This variable defines a mapping of port names to their corresponding port numbers.
It is used to specify the ports that will be allowed in the security group rules.

*Default Value:* The default map includes common ports such as SSH (port 22), mysql (port 6033), zabbix_agent (10050), zabbix_srv (10051), zabbix_front (8080).

**AMI ID Variable**

.. code-block:: terraform

    variable "ami_id" {
        default = "ami-0989fb15ce71ba39e"
        type    = string
    }

*Description:* This variable specifies the Amazon Machine Image (AMI) ID that will be used for creating the EC2 instance.
The AMI defines the base operating system and software configuration of the instance.

*Default Value:* The default value is set to an AMI ID. Customize this value based on your desired image.

**AWS Access Key Variable**

.. code-block:: terraform

    variable "AWS_ACCESS_KEY" {
        type        = string
        description = "AWS access key. Can also be set via the environment variable AWS_ACCESS_KEY_ID."
        default     = ""
        sensitive   = true
        }

*Description:* This variable is used to provide your AWS access key, which is required for Terraform to authenticate and interact with your AWS account.
The access key should have appropriate permissions.

*Default Value:* The default value is an empty string. It's recommended to provide this key securely using a secret management system.

**AWS Secret Key Variable**

.. code-block:: terraform

    variable "AWS_SECRET_KEY" {
        type        = string
        description = "AWS secret key. Can also be set via the environment variable AWS_SECRET_ACCESS_KEY."
        default     = ""
        sensitive   = true
    }

*Description:* This variable is used to provide your AWS secret key, which complements the access key for AWS authentication.
Like the access key, the secret key should be kept secure and have the necessary permissions.

*Default Value:* The default value is an empty string. Handle this secret securely, considering best practices for sensitive data.

**Private IP Variable**

.. code-block:: terraform

    variable "private_ip" {
        type    = string
        default = "172.31.43.157" #ip_private 172.31.43.157
    }

*Description:* This variable allows you to specify the private IP address that will be assigned to the EC2 instance.
Private IP addresses are used for internal communication within the VPC.

*Default Value:* The default value is set to a specific private IP address. Adjust this value based on your networking requirements.
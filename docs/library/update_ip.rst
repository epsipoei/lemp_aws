**Handle updating ip**
======================

.. toctree::
   :maxdepth: 1

   update_ip_code

The provided script is designed to automate the process of updating IP addresses for MySQL servers and related components. It allows the user to choose whether to update the public or private IP addresses.

*The script performs the following tasks:*

- Presents the user with options to update either the public or private IP address.
- Depending on the user's choice, presents a submenu for selecting the component to update.
- Retrieves the old and new IP addresses from Terraform state files.
- Modifies relevant configuration files using sed to update IP addresses.
- Displays the new IP address and a success message after the update.

**Usage**

*To use the script:*

- Run the script (main.sh) in the terminal.
- Choose whether to update the public or private IP address.
- Select the specific component (Master, Slave, Proxy, etc.).
- The script will update the IP address accordingly and display the new IP address along with a success message.
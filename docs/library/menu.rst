**Menu**
========

.. toctree::
   :maxdepth: 1

   menu_code

**Script Overview**

The provided script presents users with a main menu containing different options, such as Terraform, Ansible, Update_ip, and Manage_my_vpn. Depending on the chosen option, the script displays corresponding submenus or actions that users can further interact with.

*The script performs the following tasks:*

- Displays the main menu options to users.
- Depending on the user's choice, displays submenus for Terraform, Ansible, Update_ip, or Manage_my_vpn tasks.
- Executes corresponding actions based on user input within each submenu.

**Usage**

*To use the script:*

- Run the script in the terminal.
- Choose from the main menu options, such as Terraform, Ansible, Update_ip, or Manage_my_vpn.
- Depending on the chosen option, follow the prompts to select and execute specific tasks or actions.

**Submenu Descriptions**

- Terraform Menu: Allows users to choose a project and then select actions such as init, plan, apply, or destroy for Terraform operations.
- Ansible Menu: Provides options to execute Ansible playbooks, perform SCP operations, and activate backup for MySQL. Users can interactively choose actions and provide necessary inputs.
- Manage_my_vpn Menu: Initiates the my_vpn.sh script on a remote server to manage a WireGuard VPN setup.
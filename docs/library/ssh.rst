**SSH CP**
==========

.. toctree::
   :maxdepth: 1

   ssh_code

**Script Overview**

The provided Bash script automates the process of copying SSH keys between remote hosts.
It is particularly useful when setting up SSH key-based authentication for secure and passwordless remote access.

**Script Details**

- mysql_ip_pub: The public IP address of the MySQL host.
- backup_1_ip_priv: The private IP address of the first backup host.
- backup_1_ip_pub: The public IP address of the first backup host.
- backup_2_ip_priv: The private IP address of the second backup host.

*The script accomplishes the following steps:*

- Copies the SSH public key from the local user's home directory to the MySQL host, allowing access without a password.
- Logs into the MySQL host and copies the public key to the first backup host (backup_1_ip_priv), enabling passwordless access from the first backup host to the MySQL host.
- Logs into the first backup host and copies the public key to the second backup host (backup_2_ip_priv), establishing passwordless access from the second backup host to the first backup host.

**Benefits**

- Automation: The script automates the process of copying SSH keys, saving time and effort compared to manual setup.
- Security: SSH key-based authentication enhances security by eliminating the need for passwords, reducing the risk of unauthorized access.
- Simplified Access: Users can remotely access different hosts without repeatedly entering passwords.
- Remote Management: Admins can manage remote access and key distribution in a centralized manner.

**Precautions**

- Ensure that SSH key pairs are generated securely, and private keys are kept confidential.
- Regularly audit and manage SSH keys to maintain a secure environment.
**main.tf**
===========

**Creating an EC2 Instance**

.. code-block:: terraform

    resource "aws_instance" "my_vpn" {
      ami           = var.ami_id
      instance_type = "t3.micro"
      tags = {
        "Name" = "my_vpn"
      }
      security_groups = [aws_security_group.my_vpn.name]
      key_name        = "key_ssh"
    }

This section defines an EC2 instance with the following characteristics:

- Uses the AMI specified by the var.ami_id variable.
- Instance type: "t3.micro".
- Tag with the instance name set as "my_vpn".
- Associated with a specific security group defined by aws_security_group.my_vpn.name.
- Uses an SSH key named "key_ssh" for accessing the instance.

**Creating a Security Group**

.. code-block:: terraform

    resource "aws_security_group" "my_vpn" {
    vpc_id = var.vpc_id
    }

This part of the code creates a security group in the VPC specified by the var.vpc_id variable.

**Security Group Rules**

The code defines several security group rules to control incoming and outgoing network traffic.

**Incoming TCP Access Rules**

.. code-block:: terraform

    resource "aws_security_group_rule" "all_in_tcp" {
        for_each      = var.ports
        type          = "ingress"
        from_port     = each.value
        to_port       = each.value
        protocol      = "tcp"
        cidr_blocks   = ["0.0.0.0/0"]
        security_group_id = aws_security_group.my_vpn.id
        }

These rules allow incoming traffic on ports specified by the var.ports variable using the TCP protocol.  
Source IP addresses are limited to "0.0.0.0/0," meaning traffic is allowed from any IP address.

**Incoming UDP Access Rules**

.. code-block:: terraform

    resource "aws_security_group_rule" "all_in_udp" {
        for_each      = var.ports
        type          = "ingress"
        from_port     = each.value
        to_port       = each.value
        protocol      = "udp"
        cidr_blocks   = ["0.0.0.0/0"]
        security_group_id = aws_security_group.my_vpn.id
        }

These rules are similar to TCP rules, but they allow incoming traffic using the UDP protocol.

**Incoming ICMP Access Rule**

.. code-block:: terraform

    resource "aws_security_group_rule" "icmp_in" {
        type          = "ingress"
        from_port     = "-1"
        to_port       = "-1"
        protocol      = "icmp"
        cidr_blocks   = ["0.0.0.0/0"]
        security_group_id = aws_security_group.my_vpn.id
        }

This rule allows incoming ICMP (ping) traffic from any IP address.

**Outgoing Traffic Rule**

.. code-block:: terraform

    resource "aws_security_group_rule" "all_out" {
        type              = "egress"
        from_port         = 0
        to_port           = 0
        protocol          = "-1"
        cidr_blocks       = ["0.0.0.0/0"]
        ipv6_cidr_blocks  = ["::/0"]
        security_group_id = aws_security_group.my_vpn.id
        }  

This rule allows all outgoing traffic to any IP address and on any port, both for IPv4 and IPv6.

**Variables**

The code uses variables to parameterize the deployment. Make sure the following variables are defined and configured correctly:

- *var.ami_id:* AMI ID to use for the EC2 instance.
- *var.vpc_id:* VPC ID where the security group will be created.
- *var.ports:* List of port numbers to allow in the security group rules.
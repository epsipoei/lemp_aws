**output.tf**
=============

**Output - EC2 Instance Public IP Address**

The "output" block in Terraform is used to define values that you want to display after the infrastructure deployment is complete.
In this case, the output block is utilized to retrieve and present the public IP address of an EC2 instance that has been deployed.

.. code-block:: terraform

    output "instance_public_ip" {
        description = "Public IP of EC2 instance"
        value       = aws_instance.backup_1.public_ip
    }

*Description:* This "output" block specifies the following:

- *Description:* A human-readable description of the output's purpose, which is to display the public IP address of the EC2 instance.

- *Value:* The value to be displayed, which is obtained from the public_ip attribute of the aws_instance.backup_1 resource. This attribute holds the public IP address of the deployed EC2 instance.
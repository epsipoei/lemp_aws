**version.tf**
==============

**Terraform Configuration and AWS Provider**

The terraform block is used to configure the Terraform execution environment, including specifying the required providers and their versions.

.. code-block:: terraform

    terraform {
        required_providers {
            aws = {
              source  = "hashicorp/aws"
              version = "4.67.0"
            }
        }
    }

*Description:* This block specifies that the Terraform configuration requires the AWS provider from HashiCorp, and it sets the version to "4.67.0".

**AWS Provider Configuration**

The provider block is used to configure the AWS provider, which enables Terraform to interact with Amazon Web Services.

.. code-block:: terraform

    provider "aws" {
        region     = "eu-north-1"
        access_key = var.AWS_ACCESS_KEY
        secret_key = var.AWS_SECRET_KEY
    }

*Description:* This block configures the AWS provider with the following attributes:

- *region:* The AWS region to be used for resource deployment. In this case, it is set to "eu-north-1" (Paris region).

- *access_key:* The AWS access key used for authentication. This value is obtained from the AWS_ACCESS_KEY variable.

- *secret_key:* The AWS secret key used for authentication. This value is obtained from the AWS_SECRET_KEY variable.

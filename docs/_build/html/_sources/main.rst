**Main**
========

.. toctree::
   :maxdepth: 1

   main_code

**Script Overview**

The "EC2 Management Tool" script serves as a centralized interface for managing EC2 instances, using both Terraform and Ansible for different tasks. The script provides menus to facilitate various actions, such as deploying infrastructure, updating IP addresses, and managing specific resources.
Dependencies

*The script relies on the following components:*

- The lib/title.sh script: This script contains the print_title function that displays the program title.
- The lib/menu.sh script: This script contains the various menu-printing functions used in the script.
- The lib/handle_updating_ip.sh script: This script contains the update_mysql_ips function that handles updating IP addresses.

**Usage**

- Place all the necessary scripts and files in the appropriate directory.
- Run the main script, which will display a menu for various actions.

**Script Details**

*The script incorporates the following key features:*

- Title and Colors: The script displays a dynamic title and utilizes color codes to enhance readability.
- Main Menu: The main menu presents the user with choices to interact with Terraform, Ansible, update IP addresses, manage VPN settings, or exit the program.
- Terraform Menu: The script facilitates interaction with Terraform by presenting a sub-menu with options to initialize, plan, apply, or destroy infrastructure projects.
- Ansible Menu: The Ansible sub-menu offers choices to execute playbooks, copy files, and activate backup settings for MySQL.
- Update IP Menu: The script allows for the automation of updating IP addresses across different roles.
- Manage VPN Menu: This menu option enables management of VPN settings, triggering the execution of the my_vpn.sh script.
- Exit: The script can be exited gracefully by selecting the appropriate option.

**Benefits**

- Centralized Management: The script centralizes interactions with EC2 instances, simplifying resource management.
- Saves Time: Users can quickly perform tasks such as infrastructure deployment, updates, and IP address changes.
- Enhanced Organization: The script categorizes actions into clear menus, enhancing workflow clarity.
- Automation: The script automates tasks like IP address updates and VPN management, reducing manual effort.

**Precautions**

- Security: Ensure that AWS credentials are securely managed and stored.
- Access Control: Limit access to the script and AWS resources to authorized personnel.
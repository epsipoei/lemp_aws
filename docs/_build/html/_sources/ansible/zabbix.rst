**Role zabbix**
===============

**Package and System Update**

.. code-block:: yaml

   - name: Mettre à jour les paquets
     apt:
       update_cache: yes
       upgrade: yes
       state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module. It ensures that the server is up to date with the latest package versions.

**Install Required Packages**

.. code-block:: yaml

   - name: Install required packages
     package:
       name: "{{ item }}"
     loop:
       - mysql-server
       - language-pack-fr

*Description:* This task installs necessary packages for the Zabbix setup, including MySQL server and the French language pack.

**Copy MySQL Setup Script for Zabbix**

.. code-block:: yaml

   - name: Copy script to set up MySQL for Zabbix
     copy:
       src: ../roles/zabbix/files/scripts/mysql.sh
       dest: /usr/local/bin/mysql.sh
       mode: 'u+x'
     notify:
       - Run MySQL script for Zabbix

*Description:* This task copies the MySQL setup script for Zabbix to /usr/local/bin/mysql.sh. The script is made executable and will be used to configure the MySQL database for Zabbix.

**Copy MySQL Configuration for Zabbix**

.. code-block:: yaml

   - name: Copy MySQL configuration file for Zabbix
     copy:
       src: ../roles/zabbix/files/env/mysqld.cnf
       dest: /etc/mysql/mysql.conf.d/mysqld.cnf
       mode: '0640'

*Description:* This task copies the MySQL configuration file specific to Zabbix to /etc/mysql/mysql.conf.d/mysqld.cnf.

**Download and Install Zabbix Repository**

.. code-block:: yaml

   - name: Download Zabbix repository
     get_url:
       url: https://repo.zabbix.com/zabbix/6.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.4-1+ubuntu22.04_all.deb
       dest: /tmp/zabbix-release.deb

   - name: Install Zabbix repository
     apt:
       deb: /tmp/zabbix-release.deb

*Description:* These tasks download and install the Zabbix repository, which is required to access the Zabbix server and frontend packages.

**Install Zabbix Server and Frontend Packages**

.. code-block:: yaml

   - name: Install Zabbix server and frontend
     package:
       update_cache: yes
       name: "{{ item }}"
       state: present
     loop:
       - zabbix-server-mysql
       - zabbix-frontend-php
       - zabbix-sql-scripts

*Description:* This task installs the Zabbix server, frontend, and necessary SQL scripts using the package module.

**Set Up Nginx and Copy Configuration**

.. code-block:: yaml

   - name: Install Nginx and Zabbix Nginx configuration
     apt:
       name: zabbix-nginx-conf
       state: present

   - name: Copy Nginx configuration for Zabbix
     copy:
       src: ../roles/zabbix/files/env/nginx.conf
       dest: /etc/zabbix/nginx.conf

*Description:* These tasks install Nginx and the Zabbix-specific Nginx configuration. The Nginx configuration file for Zabbix is copied to /etc/zabbix/nginx.conf.

**Copy Zabbix Server Configuration**

.. code-block:: yaml

   - name: Copy Zabbix server configuration
     copy:
       src: ../roles/zabbix/files/env/zabbix_server.conf
       dest: /etc/zabbix/zabbix_server.conf

*Description:* This task copies the Zabbix server configuration file to /etc/zabbix/zabbix_server.conf.

**Copy Zabbix Database Schema Import Script**

.. code-block:: yaml

   - name: Copy script to import database schema
     copy:
       src: ../roles/zabbix/files/scripts/schema.sh
       dest: /usr/local/bin/schema.sh
       mode: 'u+x'
     notify:
       - Run Zabbix schema import script

*Description:* This task copies the Zabbix database schema import script to /usr/local/bin/schema.sh. The script is made executable and will be used to import the Zabbix database schema.

**Copy PHP-FPM Configuration**

.. code-block:: yaml

   - name: Copy PHP-FPM configuration
     copy:
       src: ../roles/zabbix/files/env/php-fpm.conf
       dest: /etc/zabbix/php-fpm.conf

*Description:* This task copies the PHP-FPM configuration file specific to Zabbix to /etc/zabbix/php-fpm.conf.

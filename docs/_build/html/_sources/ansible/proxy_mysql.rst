**Role proxy_mysql**
====================

**Package and System Update**

.. code-block:: yaml

    name: Mettre à jour les paquets
    apt:
      update_cache: yes
      upgrade: yes
      state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module. 
It ensures that the server is up to date with the latest package versions.

**Install HAProxy Packages**

.. code-block:: yaml

   - name: Install haproxy packages
     package:
       name: "{{ item }}"
     loop:
       - haproxy

*Description:* This task installs HAProxy using the package module. 
It ensures that the HAProxy software is properly installed on the server for load balancing and proxying.

**HAProxy Configuration**

.. code-block:: yaml

   - name: Copy haproxy.cfg
     copy:
       src: ../roles/proxy_mysql/files/haproxy.cfg
       dest: /etc/haproxy/haproxy.cfg
       mode: '0644'
     notify:
       - Restart haproxy

*Description:* This task copies the HAProxy configuration file haproxy.cfg to the /etc/haproxy/ directory. 
The file contains the configuration settings for HAProxy's behavior, load balancing algorithms, and proxy rules. The notify directive triggers a handler to restart the HAProxy service.

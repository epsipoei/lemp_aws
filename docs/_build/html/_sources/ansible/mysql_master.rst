**Role mysql_master**
=====================

**Package and System Update**

.. code-block:: yaml

   - name: Mettre à jour les paquets
     apt:
       update_cache: yes
       upgrade: yes
       state: present

*Description:* This task updates the package cache and upgrades the system packages using the apt module.
It ensures that the server is up to date with the latest package versions.

**Install MySQL Packages**

.. code-block:: yaml

   - name: Install MySQL packages
     package:
       name: "{{ item }}"
     loop:
       - mysql-server

*Description:* This task installs MySQL server package using the package module.
It ensures that MySQL is properly installed on the server.

**MySQL Setup**

.. code-block:: yaml

   - name: Create user and db mysql
     copy:
       src: ../roles/mysql/files/mysql.sh
       dest: /usr/local/bin/mysql.sh
       mode: 'u+x'
     notify:
       - Run MySQL script

   - name: Create Schema mysql
     copy:
       src: ../roles/mysql/files/db_mspr.sh
       dest: /usr/local/bin/db_mspr.sh
       mode: 'u+x'
     notify:
       - Run Schema MySQL script

   - meta: flush_handlers

   - name: Copy conf mysql
     copy:
       src: ../roles/mysql/files/my.cnf
       dest: /etc/mysql/my.cnf
       mode: '0644'
     notify:
       - Restart MySQL

   - meta: flush_handlers

*Description:* These tasks configure MySQL by copying necessary scripts and configuration files.
The MySQL setup includes creating users, databases, schemas, and copying configuration files. Handlers are used to manage notifications for the execution of certain scripts and configurations.

**Generate MySQL Backup**

.. code-block:: yaml

   - name: Generate MySQL backup
     become: true
     shell: mysqldump --databases mspr --single-transaction --triggers --routines > dump.sql
     register: dump_output
     args:
       executable: /bin/bash

*Description:* This task generates a MySQL backup using the mysqldump command. It backs up the "mspr" database along with triggers and routines.
The backup is stored in the dump.sql file.

**Group and User Creation**

.. code-block:: yaml

   - name: Create a new group
     group:
       name: back
       state: present

   - name: Add user to 'backup' group
     user:
       name: backy
       state: present
       groups: back
       password: "{{ 'iopiop' | password_hash('sha512', 'mysecretsalt') }}"
       shell: /bin/bash

*Description:* These tasks create a new group named "back" and add a user named "backy" to the "backup" group.
The user's password is securely hashed using SHA-512. These steps ensure the presence of appropriate user and group entities for backup purposes.

**SSH and Configuration Management**

.. code-block:: yaml

   - name: Copy conf ssh
     copy:
       src: ../roles/mysql/files/sshd_config
       dest: /etc/ssh/sshd_config
       mode: '0644'
     notify:
       - Redémarrer le service SSH

   - meta: flush_handlers

   - name: Vérifier si la clé existe déjà
     stat:
       path: /home/backy/.ssh/id_ed25519
     register: key_file

   - name: Copy conf ssh
     copy:
       src: ../roles/mysql/files/keygen.sh
       dest: /usr/local/bin/keygen.sh
       mode: '0755'
     notify:
       - Run keygen
     when: not key_file.stat.exists

   - meta: flush_handlers

*Description:* These tasks configure SSH settings by copying SSH configuration files and managing SSH keys.
The notify directive triggers handlers for SSH service restart and key generation.

**Backup Directory and Cron Jobs**

.. code-block:: yaml

   - name: Create share directories
     file:
       path: /tmp/backup
       state: directory
       owner: backy
       group: back
       mode: '0755'

   - name: Copy backup.sh
     copy:
       src: ../roles/mysql/files/backup.sh
       dest: /usr/local/bin/backup.sh
       owner: backy
       group: back
       mode: '0755'

   - name: Copy crontab file
     copy:
       src: ../roles/mysql/files/crontab.conf
       dest: /etc/cron.d/cron_backup
       owner: backy
       group: back
       mode: '0755'

   - name: Set cron permissions
     file:
       path: /etc/cron.d/cron_backup
       mode: '0755'

   - name: Update cron tasks
     cron:
       name: cron_backup
       user: root
       job: /usr/bin/crontab /etc/cron.d/cron_backup

*Description:* These tasks create directories for storing backups, copy backup scripts, and set up cron jobs for automated backup scheduling.
Cron jobs are configured to run the backup.sh script periodically.
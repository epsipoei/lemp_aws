.PHONY: lib run docs dependency setup

lib:
	pip3 install --user -r requirements.txt

run:
	chmod +x main.sh
	./main.sh

docs:
	firefox docs/_build/html/index.html &

dependency:
	chmod +x dependency.sh
	./dependency.sh

setup:
	python3 setup.py
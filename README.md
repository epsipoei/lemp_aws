<!-- markdownlint-disable-file MD033 MD041-->
<div align="center">

# **EC2 Management Tool**

</div>

This GitHub repository contains an EC2 management tool that allows you to manage EC2 servers on AWS. The tool is developed in shell for ease of use.

## **Summary**

[[*TOC*]]

### **Functionality**

The tool uses files in the /lib directory to provide the necessary functionalities. The functionalities are presented in a main menu that helps the user interact with the tool. Each option in the main menu offers a functionality to manage EC2 servers.

The options in the main menu are:

    terraform       : allows managing Terraform infrastructures
    ansible         : allows managing Ansible configurations
    update_ip_mysql : allows updating MySQL server IP addresses
    exit            : allows quitting the tool

For each selected option, the user will be directed to a submenu to manage that specific task.

### **Usage**

The tool can be used by running the command `./main.sh`. This will display the main menu, and the user can select the desired option from there.

  - Install dependencies

        make lib

  - Run tool

        make run    

It is important to note that this tool requires valid AWS credentials to function correctly. Make sure you have configured the authentication credentials before using the tool.

#### **Method of Usage for Creating Your Infrastructure**

Prerequisites

- You must have key ssh format ed25519 in your .ssh

        ssh-keygen -t ed25519 -C "user@my_mail.net"

- You must have AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY define in your .bashrc

        echo 'export AWS_ACCESS_KEY_ID="your key"' >> ~/.bashrc
        echo 'export AWS_SECRET_ACCESS_KEY="your secret"' >> ~/.bashrc

- You must define your vpc-id in your .bashrc
  
        echo 'export TF_VAR_vpc_id="your vpc_id"' >> ~/.bashrc

#### **Run main.sh**

**First Time for Each Project:**

- 1- Select Terraform  
  - 1-select Project  
    - 1-init  
    - 2-apply  

**After :**

- 2- Update_ip
  - 1- Public_IP
    - 1- Master
    - 2- Slave
    - 3- Proxy
    - etc ...
  - 2- Private_IP
    - 1- Master
    - etc ...

- 3- Select Ansible
  - 1- Exec_playbook  
    - 1- deploy_prj_mysql_master

- 4- Select Exec_scp_dump

- 5- Select Ansible  
  - 1-Exec_playbook  
    - 1- deploy_prj_mysql_slave
    - 2- deploy_prj_proxy_mysql
    - etc ...

- 6- Select Ansible
  - 1- Activate_Backup_Mysql

- 7- Manage_my_vpn

### Documentation

    make docs
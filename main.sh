#!/bin/bash

TITLE="EC2 Management Tool"
projects=("prj_mysql_master" 
          "prj_mysql_slave" 
          "prj_proxy_mysql" 
          "prj_srv_zabbix"
          "prj_my_vpn"
          "prj_backup_1" 
          "prj_backup_2"
          "Return")
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m'

source lib/title.sh
source lib/menu.sh
source lib/handle_updating_ip.sh

# Main function
main() {
  local choice

  while true; do
    clear
    print_title
    print_instructions
    print_main_menu
    printf "\nInput Your choice: " 
    read choice

    case $choice in
      1|terraform)
        clear
        print_title
        print_terraform_menu
        ;;
      2|ansible)
        clear
        print_title
        print_ansible_menu
        ;;
      3|update_ip)
        clear
        print_title
        update_mysql_ips
        ;;
      4|manage_my_vpn)
        clear
        print_my_vpn_menu
        ;;
      5|exit)
        echo -e "\n${BLUE}Goodbye!${NC}\n"
        break
        ;;
      *)
        echo -e "\nInvalid choice. Please try again.\n"
        ;;
    esac
  done
}

# Call the main function
main

